# pihole

Custom domain blocking for my pihole

## Installation from scratch

* Install latest Raspbian image
* Set up local, a reserved DHCP address, etc.
* Check for any apt-get updates/upgrades
* `sudo reboot`
* Install Pi-hole with `curl -sSL https://install.pi-hole.net | bash`

* Install and configure Cloudflare DNS over HTTPS https://docs.pi-hole.net/guides/dns-over-https/

## PADD / Other
* https://github.com/jpmck/PADD
* https://github.com/bpennypacker/phad

## Pihole Exporter for Prometheus
* [GitHub Project](https://github.com/eko/pihole-exporter)
* [Grafana Dashboard](https://grafana.com/grafana/dashboards/10176)

### Install
* `curl -SL https://github.com/eko/pihole-exporter/releases/latest/download/pihole_exporter-linux-arm > pihole-exporter && chmod +x pihole-exporter && sudo mv pihole-exporter /usr/local/bin/`
* `sudo nano /etc/systemd/system/piholeexporter.service`
* Add service
```
[Unit]
Description=PiholeExporter

[Service]
TimeoutStartSec=0
ExecStart=/usr/local/bin/pihole-exporter

[Install]
WantedBy=multi-user.target
```
* Restart daemon and start service
```
sudo systemctl daemon-reload \
 && sudo systemctl enable piholeexporter \
 && sudo systemctl start piholeexporter
```
* Go to http://host:9617/metrics

## Useful Items
* [What files does Pi-hole use?](https://discourse.pi-hole.net/t/what-files-does-pi-hole-use/1684)
* 